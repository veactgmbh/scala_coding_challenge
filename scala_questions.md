# Scala Questions

## Explain the difference: 
* `map` vs `foreach`.
* Give a single example where map and another one where foreach is to be preferred.
* what does the projection to a for-loop look like for these two

## Type inference
do not use the repls
```scala
    val foo = if (true) 5
```

* what type is foo? Explain why ?


## How to model multiple constructors in Scala ? 
## What is a “Companion Object” ?
## Case classes
* What do they provide for free ?
* What should you not do when using a case class ?
* What are they commonly used for ?

## Futures

a)
```scala
val f1 = Future(Thread.sleep(100))
val f2 = Future(Thread.sleep(100))

val a = for {
 _ <- f1
 _ <- f2
} yield { () }

Await.result(a, Duration(150, TimeUnit.MILLISECONDS))

```

b)
```scala
val b = for {
 _ <- Future( Thread.sleep(100))
 _ <- Future( Thread.sleep(100))
} yield { () }

Await.result(b, Duration(150, TimeUnit.MILLISECONDS))
```

* what will be the output of a) ?
* what will be the the output of b) ?
* In what regard does a differ to b ?
* When to use a) when to use b)